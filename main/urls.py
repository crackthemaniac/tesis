"""main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from mantenedor import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('crear_docente/',views.crearDocente, name='crear_docente'),
    path('listar_docente/',views.listarDocente, name='listar_docente'),
    path('email/',views.Email, name='email'),
    path('editar_docente/<id_profesor>',views.editarDocente, name='editar_docente'),
    path('eliminar_docente/<id_profesor>',views.eliminarDocente, name='eliminar_docente'),
    path('eliminar_todo/',views.eliminarTodo, name='eliminar_todo'),
    path('import',views.simple_upload, name='import'),
    path('contact/<id_profesor>',views.contact, name='contact'),
    path('reporte_online',views.reporteOnline, name='reporte_online'),
    path('confirmar_eliminar_todo/', views.confirmar_eliminar_todo, name='confirmar_eliminar_todo'),
    path('acuso_recibo',views.acusoRecibo, name='acuso_recibo'),
    path('reporte/<id_profesor>',views.GeneratePdf.as_view(), name='reporte'),
    path('enviarEmail/<id_profesor>',views.enviarEmail, name='enviarEmail'),
]
