from django.urls import path
from mantenedor.views import reporteFinal,index, eliminarTodo, crearDocente, listarDocente, editarDocente, eliminarDocente, DocenteList, contact, GeneratePdf, reporteOnline, Email, enviarEmail, enviarTodo, confirmar_eliminar_todo


urlpatterns = [
    path('index/', index, name='index'),
    path('crear_docente/', crearDocente, name='crear_docente'),
    path('listar_docente/', listarDocente, name='listar_docente'),
    path('editar_docente/<id_profesor>',editarDocente, name='editar_docente'),
    path('eliminar_docente/<id_profesor>',eliminarDocente, name='eliminar_docente'),
    path('eliminar_todo/',eliminarDocente, name='eliminar_todo'),
    path('email/',Email, name='email'),
    path('import/',simple_upload, name='import'),
    path('contact/<id_profesor>',contact, name='contact'),
    path('acuso_recibo/', acusoRecibo, name='acuso_recibo'),
    path('reporte_online/',reporteOnline, name='reporte_online'),
    path('reporte/<id_profesor>',GeneratePdf.as_view(), name='reporte'),
    path('confirmar_eliminar_todo/', confirmar_eliminar_todo, name='confirmar_eliminar_todo'),
    path('enviarEmail/<id_profesor>',enviarEmail, name='enviarEmail'),
]
