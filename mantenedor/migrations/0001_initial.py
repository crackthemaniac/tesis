# Generated by Django 2.1.5 on 2019-10-17 16:21

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=20)),
                ('email', models.EmailField(max_length=144)),
                ('mensaje', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Contador3',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_docente', models.CharField(max_length=100, unique=True)),
                ('contador', models.PositiveIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Profesor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('RUT', models.CharField(max_length=20)),
                ('nombre_docente', models.CharField(max_length=100)),
                ('nivel_formacion_academica', models.CharField(max_length=50)),
                ('tipo_contrato', models.CharField(max_length=30)),
                ('tipo_actividad', models.CharField(max_length=30)),
                ('periodo', models.CharField(max_length=30)),
                ('sede', models.CharField(max_length=30)),
                ('campus', models.CharField(max_length=30)),
                ('facultad_programa', models.CharField(max_length=30)),
                ('programa', models.CharField(max_length=30)),
                ('nombre_curso', models.CharField(max_length=50)),
                ('NRC', models.CharField(max_length=30)),
                ('n_alumnos', models.IntegerField()),
                ('n_alumnos_responden', models.IntegerField()),
                ('respuestas', models.DecimalField(decimal_places=0, max_digits=5)),
                ('P7', models.DecimalField(decimal_places=0, max_digits=5)),
                ('P1', models.DecimalField(decimal_places=0, max_digits=5)),
                ('P2', models.DecimalField(decimal_places=0, max_digits=5)),
                ('P3', models.DecimalField(decimal_places=0, max_digits=5)),
                ('P4', models.DecimalField(decimal_places=0, max_digits=5)),
                ('P5', models.DecimalField(decimal_places=0, max_digits=5)),
                ('P6', models.DecimalField(decimal_places=0, max_digits=5)),
                ('evaluacion_promedio', models.DecimalField(decimal_places=0, max_digits=5)),
                ('promedio_nota', models.IntegerField()),
                ('email', models.EmailField(max_length=144)),
            ],
        ),
    ]
