# Generated by Django 2.1.5 on 2019-10-17 16:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mantenedor', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('RUT', models.CharField(max_length=20, unique=True)),
                ('contador', models.PositiveIntegerField(default=0)),
            ],
        ),
        migrations.DeleteModel(
            name='Contador3',
        ),
    ]
