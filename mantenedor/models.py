from django.db import models

# Create your models here.


class Profesor(models.Model):
	RUT = models.CharField(max_length=20)
	nombre_docente = models.CharField(max_length=100)
	nivel_formacion_academica = models.CharField(max_length=50)
	tipo_contrato = models.CharField(max_length=30)
	tipo_actividad = models.CharField(max_length=30)
	periodo = models.CharField(max_length=30)
	sede = models.CharField(max_length=30)
	campus = models.CharField(max_length=30)
	facultad_programa = models.CharField(max_length=30)
	programa = models.CharField(max_length=30)
	nombre_curso = models.CharField(max_length=50)
	NRC = models.CharField(max_length=30)
	n_alumnos = models.IntegerField()
	n_alumnos_responden = models.IntegerField()
	respuestas = models.DecimalField(max_digits=5, decimal_places=0)
	P7 = models.FloatField()
	P1 = models.FloatField()
	P2 = models.FloatField()
	P3 = models.FloatField()
	P4 = models.FloatField()
	P5 = models.FloatField()
	P6 = models.FloatField()
	evaluacion_promedio = models.FloatField()
	promedio_nota = models.FloatField()
	email = models.EmailField(max_length=144)

	def cambiar_porcentaje(self):
		self.P1 = round(self.P1,2)
		self.P2 = round(self.P2,2)
		self.P3 = round(self.P3,2)
		self.P4 = round(self.P4,2)
		self.P5 = round(self.P5,2)
		self.P6 = round(self.P6,2)
		self.P7 = round(self.P7,2)
		self.respuestas = round(self.respuestas,2)
		self.evaluacion_promedio = round(self.evaluacion_promedio,2)

		self.P1 = self.P1 * 100
		self.P2 = self.P2 * 100
		self.P3 = self.P3 * 100
		self.P4 = self.P4 * 100
		self.P5 = self.P5 * 100
		self.P6 = self.P6 * 100
		self.P7 = self.P7 * 100
		self.respuestas = self.respuestas * 100
		self.evaluacion_promedio = self.evaluacion_promedio * 100
		return self

	def __str__(self):
		return self.RUT

	

class Contador(models.Model):
	RUT = models.CharField(max_length=20, unique=True)
	contador = models.PositiveIntegerField(default=0)

	def aumentar_contador(self):
		self.contador += 1
		self.save()




		